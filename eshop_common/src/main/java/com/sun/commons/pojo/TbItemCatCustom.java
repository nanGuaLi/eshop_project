package com.sun.commons.pojo;

import com.sun.webmanage.model.TbItemCat;

public class TbItemCatCustom extends TbItemCat{

	private TbItemCatCustom child;
	
	private int depth;

	public TbItemCatCustom getChild() {
		return child;
	}

	public void setChild(TbItemCatCustom child) {
		this.child = child;
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}
	
}
