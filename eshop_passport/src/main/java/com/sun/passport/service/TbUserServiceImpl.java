package com.sun.passport.service;

import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.sun.commons.utils.ActionUtils;
import com.sun.commons.utils.JsonUtils;
import com.sun.dubbo.webmanage.service.TbUserDubboService;
import com.sun.exception.DaoException;
import com.sun.redis.dao.RedisDao;
import com.sun.webmanage.model.TbUser;

@Service("TbUserService")
public class TbUserServiceImpl implements TbUserService{

	@Reference
	private TbUserDubboService tbUserDubboService;
	@Resource
	private RedisDao redisDao;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TbUserService.class);

	@Override
	public TbUser loadTbUserByUsernameAndPassword(String username, String password) {
		return tbUserDubboService.loadTbUserByUsernameAndPassword(username, password);
	}

	@Override
	public boolean saveTbUser(TbUser tbUser) {
		try {
			boolean insertTbUser = tbUserDubboService.insertTbUser(tbUser);
		    return insertTbUser;
		} catch (DaoException e) {
			e.printStackTrace();
			LOGGER.error("保存用户信息异常："+e.getMessage());
		}
		return false;
	}

	@Override
	public Map<String,Object> getUserInfoByToken(String token) {
		String infoJson = redisDao.getKey(token);
		if(infoJson!=null && !infoJson.equals("")){
			TbUser pojo = JsonUtils.jsonToPojo(infoJson, TbUser.class);
			pojo.setPassword(null);
			return ActionUtils.ajaxSuccess(null, pojo);
		}
		return ActionUtils.ajaxFail(null, null);
	}
	
}
